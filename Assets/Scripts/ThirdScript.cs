﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class ThirdScript : MonoBehaviour
    {
        public bool FirstVar;
        public bool SecondVar;
        public bool ThirdVar;

        void Update()
        {
            FirstMethod();
            SecondMethod();
        }

        void FirstMethod()
        {
            if (FirstVar && SecondVar && ThirdVar)
            {
                Debug.Log("All values are true");
            }
            else if ((FirstVar && SecondVar) || (SecondVar && ThirdVar))
            {
                Debug.Log("Only two values are true");
            }
        }

        void SecondMethod()
        {
            if (FirstVar)
            {
                Debug.Log("First variable is true");
            }
        }
    }
}