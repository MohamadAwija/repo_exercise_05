﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class FirstScript : MonoBehaviour
    {
        public int Health = 100;
        public bool isAlive;

        void Update()
        {
            if (isAlive)
            {
                Debug.Log(Health);

                if (Health == 10)
                {
                    Debug.Log("Player has 10 health points");
                }

                if (Health < 10)
                {
                    Debug.Log("Player has less than 10 health points");
                }
            }
        }
    }
}