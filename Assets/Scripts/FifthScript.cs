﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class FifthScript : FourthScript
    {
        public Playerstate playerstate;
        private float playerMovement;

        private void Update()
        {
            switch (playerstate)
            {
                case Playerstate.NONE:
                    playerMovement = 0;
                    Debug.Log(playerMovement);
                    Debug.Log(playerstate);
                    break;

                case Playerstate.IDLE:
                    playerMovement = 0;
                    Debug.Log(playerMovement);
                    Debug.Log(playerstate);
                    break;

                case Playerstate.WALKING:
                    playerMovement = 3;
                    Debug.Log(playerMovement);
                    Debug.Log(playerstate);
                    break;

                case Playerstate.RUNNING:
                    playerMovement = 7;
                    Debug.Log(playerMovement);
                    Debug.Log(playerstate);
                    break;
            }
        }
    }
}